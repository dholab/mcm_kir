#!/usr/bin/python
#bbtools Repair.sh class
#1/7/2015
#Michael Graham

from subprocess import call
import subprocess
import os
import time
import datetime

class bbRepair():
	
	def __init__(self,outputPath,r1File,r2File,repairExecPath,logfilePath):
		"""
		Constructor
		"""
		self.outputPath = outputPath
		self.r1File = r1File
		self.r2File = r2File
		self.repairExecPath = repairExecPath
		self.logfilePath = logfilePath
	
	def log(self,message):
		if not os.path.isdir(self.logfilePath + '/log'):
			os.mkdir(self.logfilePath + '/log')
		with open(self.logfilePath + "/log/log.txt","a") as log:
			log.write(message)
			log.write("\n")
	
	def repair(self):
		call([self.repairExecPath,'in=' + self.r1File,'in2=' + self.r2File,'out=' + self.outputPath + "/R1_sorted.fq",'out2=' + self.outputPath + "/R2_sorted.fq",'outsingle=' + self.outputPath + "/singletons.fq","-Xmx4g"])
		cmdString = self.repairExecPath + ' in=' + self.r1File + ' in2=' + self.r2File + ' out=' + self.outputPath + "/R1_sorted.fq" + ' out2=' + self.outputPath + "/R2_sorted.fq" + ' outsingle=' + self.outputPath + "/singletons.fq" + " -Xmx4g"
		self.log(cmdString)
		
if __name__ == "__main__":
	repairer = bbRepair("output","R1_100k.fq.gz","R2_100k.fq.gz","bbmap33.37b/repair.sh")
	repairer.repair()
