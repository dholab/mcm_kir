#!/usr/bin/python

from subprocess import call
import re
from CoverageDepth import CoverageDepth
import argparse
import os
import glob
from Repair import bbRepair
import time
import datetime

class RhexomeProcess():
	
	def __init__(self,outputPath,minReadsPerBase,readsPath,referenceBedFile,exonsFile):
		"""
		Constructor
		"""
		self.outputPath = outputPath
		self.minReadsPerBase = minReadsPerBase
		self.readsPath = readsPath
		self.referenceBedFile = referenceBedFile
		self.exonsFile = exonsFile
		
	def log(self,message):
		if not os.path.isdir(self.outputPath + '/log'):
			os.mkdir(self.outputPath + '/log')
		with open(self.outputPath + '/log/log.txt','a') as logFile:
			logFile.write(message)
			logFile.write('\n')

	def get_immediate_subdirectories(self,a_dir):
		return [name for name in os.listdir(a_dir) if os.path.isdir(os.path.join(a_dir, name))]
            
	def getR1R2(self,samplePath):
		fileNames = glob.glob(samplePath + '/*.fastq.gz')
	
		return fileNames
	
	def processSampleData(self):
                       
		runDirectoryList = self.get_immediate_subdirectories(self.readsPath)

		for directoryName in runDirectoryList:
			if directoryName[:1] != "." and directoryName[:1] == "C":
				
				r1r2 = self.getR1R2(self.readsPath + '/' + directoryName)
				repairer = bbRepair(self.readsPath + '/' + directoryName,r1r2[0],r1r2[1],'bbmap/repair.sh',self.outputPath)
				repairer.repair()
				r1sortedPath = self.readsPath + '/' + directoryName + '/R1_sorted.fq'
				r2sortedPath = self.readsPath + '/' + directoryName + '/R2_sorted.fq'
				self.log("Starting sample number: " + directoryName)
				
				self.log(r1sortedPath + ' and ' + r2sortedPath + ' sorted by repair.sh')
			
				call(['bbmap/bbmap.sh','in=' + r1sortedPath,'in2=' + r2sortedPath,'ref=' + self.exonsFile,'outm=' + self.outputPath + '/mapping_results.bam',
				'semiperfectmode=t','ambiguous=all','rescue=f','qtrim=lr','-Xmx4g'])
				endTimeBaited = datetime.datetime.now().strftime("%m/%d/%Y_%H:%M:%S")
				self.log("Read mapping complete at: " + endTimeBaited)
		
				# filter results by read depth
				cov = CoverageDepth(self.outputPath + '/mapping_results.bam',self.referenceBedFile,self.outputPath,self.minReadsPerBase,directoryName)
				cov.generateCoverageFiles()
				cov.filterCoverageDepth()
				endTimeDepth = datetime.datetime.now().strftime("%m/%d/%Y_%H:%M:%S")
				self.log("Read depth filtering complete at: " + endTimeDepth + "\n")
				
	def parseOutput(self):
		resultsFileNames = glob.glob(self.outputPath + '/*.txt')

		# read through each file, build dictionary that includes sample number
		#		loop over dictionary, spewing contents to a concatenated results file
		resultsDictionary = {}

		for fname in resultsFileNames:
			match = re.search('(\w+-?\d+-?\w+)_1',fname)
			sampleName = match.group(1)
			with open(fname,'r') as outputSubFile:
				for line in outputSubFile:
					resultLine = line.split()
					alleleName = resultLine[0]
					#lose the exon tag
					alleleBaseName = alleleName.split('&')[0]
					readCount = resultLine[3]
			
					if sampleName not in resultsDictionary:
						resultsDictionary[sampleName] = {alleleBaseName : [readCount]}
					elif alleleBaseName not in resultsDictionary[sampleName]:
						resultsDictionary[sampleName][alleleBaseName] = []
						resultsDictionary[sampleName][alleleBaseName].append(readCount)
					else:
						resultsDictionary[sampleName][alleleBaseName].append(readCount)
		self.writeOutput(resultsDictionary)
		
	def writeOutput(self,resultsDictionary):
		#########
		#Write out pivot table like output as tab separated text
		#########
		allAlleleNames = []

		for sampleName in resultsDictionary:
			for alleleName in resultsDictionary[sampleName]:
				if alleleName not in allAlleleNames:
					allAlleleNames.append(alleleName)
			
		aggregatedOutputFile = open(self.outputPath + '/combined_results.txt','w')
		aggregatedOutputFile.write('Allele Name\t')

		for sampleName in resultsDictionary:
			aggregatedOutputFile.write(sampleName + '\t')
	
		aggregatedOutputFile.write('\n')
			
		#sort allele names to try and group A's and B's
		for name in sorted(allAlleleNames):
			aggregatedOutputFile.write(name + '\t')
	
			for sample in resultsDictionary:
				if name in resultsDictionary[sample]:
					for exonNum in range(len(resultsDictionary[sample][name])):
						aggregatedOutputFile.write(resultsDictionary[sample][name][exonNum] + '  ')
					aggregatedOutputFile.write('\t')
				else:
					aggregatedOutputFile.write('\t')
			
			aggregatedOutputFile.write('\n')
	
if __name__ == "__main__":	
	parser = argparse.ArgumentParser()
	parser.add_argument("-o", metavar="output_directory",help="Specify an output folder path", type=str,required = True)
	parser.add_argument("-reads", metavar="FOLDER",help="Specify a directory containing read directories", type=str,required = True)
	parser.add_argument("-bed", metavar="FOLDER",help="Specify a bed file defining the size of exon sequences", type=str,default="ref_files/KIRexons.bed")
	parser.add_argument("-exonsRef", metavar="FOLDER",help="Specify an exons reference fasta file", type=str,default="ref_files/KIRexons.fasta")
	
	args = parser.parse_args()
	referenceBedFile = args.bed
	exonsFile = args.exonsRef
		
	processor = RhexomeProcess(args.o,1,args.reads,referenceBedFile,exonsFile)
	
	processor.log("Started KIR exome genotyping analysis process ")
	startTime = datetime.datetime.now().strftime("%m/%d/%Y_%H:%M:%S")
	processor.log(startTime + '\n')
	processor.processSampleData()
	processor.parseOutput()

	endTime = datetime.datetime.now().strftime("%m/%d/%Y_%H:%M:%S")
	processor.log("\nKIR exome genotyping analysis process completed: " + endTime)
	
